# MyLatino
MyLatino è un'applicazione ideata per avere la grammatica latina sempre a portata di mano. Declinazioni, verbi, pronomi e tanto altro ancora.

## Download (Android)
https://play.google.com/store/apps/details?id=it.justpeppinus.mylatinoultimate

## Segnalazione errori o problemi
Per segnalare eventuali errori o problemi all'interno dell'app, è possibile farlo cliccando nel menù a sinistra su <b>Issues</b> e successivamente <b>New issues</b>. <br>
Tutte le segnalazioni saranno prese in considerazione per i successivi aggiornamenti.

### Sviluppatore
Giusepe Criscione

### Supporto
Alessio Ruta
